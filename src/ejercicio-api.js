import { LitElement, html, css } from 'lit-element';

export class EjercicioApi extends LitElement {
	static get styles() {
		return css`
			div{	border:2px solid;	border-color:#001E38; font-family:'Curier New', monospace; 
				}
			div header{	color:#eeeeee;	font-family:'Curier New', monospace; background:#5f8fb0;
				}
			div section{ 
				margin:5px;
				}
			table{	border-collapse: collapse;	width: 100%;	font-family:'Curier New', serif;
				}
			table td, th {	border: 1px solid #ddd;	padding: 5px;
				}
			table tr:nth-child(even){ background-color: #d8f6ea;
				}
			table tr:hover {	background-color:#093b53; color:rgb(249, 250, 250);
				}
			table th {	padding-top: 5px;	padding-bottom: 5px;	text-align: left;	background-color: #172028; color: white; 	
				}
			thead{	border-radius: 8px;
				}
		`;
	}

	static get properties(){
		return {
			cadena: 	{ type: String },
			arreglo:	{ type: Array },
			estado: 	{ type: Boolean },
		}
	}

	constructor(){
		super();
      this.cadena = '';
      this.arreglo = [];
			this.estado= true;
    
	}

  consultaApi(){
    fetch('http://dummy.restapiexample.com/api/v1/employees')
      .then(response => response.json())
        .then(datas => { this.arreglo = datas.data });
		//console.log(this.arreglo)
		//console.log(this.arreglo.length) 
	}

  llenarTabla(){
    this.consultaApi();
    console.log('llenar tabla')
		return this.arreglo.map(empleados => html`
			<tr>
				<td>${empleados.id}</td>
				<td>${empleados.employee_name}</td>
				<td>${empleados.employee_age}</td> 
				<td>${empleados.employee_salary}</td>
			</tr>
		`)
	}

	llenarBusqueda(){
    console.log('llenar busqueda')
		//this.cadena ='Tiger Nixon'
		this.consultaApi();
		this.cadena = this.shadowRoot.querySelector("#inputBuscar").value;
		return html`
				${this.arreglo.map(busqueda => busqueda.employee_name.includes(this.cadena) ? html`
				<tr>
					<td>${busqueda.id}</td>
					<td>${busqueda.employee_name}</td>
					<td>${busqueda.employee_age}</td>
					<td>${busqueda.employee_salary}</td>
				</tr>`
				:'')}
		`;
	}

	estadoBscar(){
		this.estado = false;
		this.render();
	}

	estadoLimpiar(){
		this.cadena ="";
		this.estado = true
		this.shadowRoot.querySelector("#inputBuscar").value = this.cadena;
		this.render();
	}

	render() {
		return html`
			<div>
				<header>Ejercicio LitElement-Api</header>
				<section>
					<label>Buscar</label>
					<input id="inputBuscar" name="inputBuscar" placeholder="Escribe aqui">
					<button id="btnBuscar" @click="${this.estadoBscar}">Buscar</button>
					<button id="btnLimpiar" @click="${this.estadoLimpiar}">Clear</button>
				</section>
				<table>
					<thead>
						<tr>
							<th>Id</th>
							<th>Nombre</th>
							<th>Edad</th>
							<th>Salario</th>
						</tr>
					</thead>
					<tbody>
						${this.estado? 
						html` ${this.llenarTabla()}` :
						html `${this.llenarBusqueda()}`}
					</tbody>
				</table>
			</div>
		`;
	}
}

customElements.define('ejercicio-api', EjercicioApi);